package com.daxing.simple;

import com.daxing.utils.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.nio.charset.StandardCharsets;

public class Sender {
    private static final String QUEUENAME = "hansg";

    public static void main(String[] args) throws Exception {
        //获取连接
        Connection connection = ConnectionUtils.getConnection();
        //声明通道,就类似于数据库的statment
        Channel channel = connection.createChannel();
        //声明队列
        /*
        参数1 队列的名字
        参数2 是否是持久化的队列,如果是,会保存到磁盘上
        参数3 是否排外,当设置为true不允许同时多个连接,否则会抛异常
        参数4 自动删除,当设置为true的时候,在队列没有未消费的消息并且没有客户端连接的情况下会自动删除
        参数5 一些额外的创建参数
         */
        channel.queueDeclare(QUEUENAME, false, false, false, null);
        //发送消息
        String msg = "只有在最低谷的时候，才能看清所有人真正的样子";
        for (int i = 0; i < 100; i++) {
            channel.basicPublish("", QUEUENAME, null, (msg+i).getBytes(StandardCharsets.UTF_8));

        }
        //如果用不到就可以关闭了
        channel.close();
        connection.close();
    }
}
