package com.daxing.rabbitmq;

import com.daxing.utils.ConnectionUtils;
import com.rabbitmq.client.*;

import java.io.IOException;


public class Consumer3 {
    private static final String QUEUENAME = "routingQueue3";
    private static final String EXCHANGENAME = "routingexchange";


    public static void main(String[] args) throws Exception {

        Connection connection = ConnectionUtils.getConnection();
        Channel channel = connection.createChannel();
        //有可能我们的消费者启动的时候生产者还没有启动,所以可能没有队列,所以消费者也需要声明队列
        //队列可以声明多次,但是实际上只会创建一次,并且注意 多次声明的参数必须一模一样
        channel.queueDeclare(QUEUENAME, false, false, false, null);
        //因为生产者和消费者谁先执行不确定,所以每个程序都必须声明交换机
        channel.exchangeDeclare(EXCHANGENAME, "direct");

        //告诉消费者从哪个交换机中获取数据,将队列绑定到交换机,routingkey就是告诉mq我想要什么消息
        channel.queueBind(QUEUENAME, EXCHANGENAME, "siliao");
        channel.queueBind(QUEUENAME, EXCHANGENAME, "saoma");

        /*
        参数1 从哪个队列中收消息
        参数2 是否自动应答,我们需要应道消息服务器我们有没有收到消息,如果不应该,服务器会认为消息我们没有收到,为了保证消息的可靠性,会不断重试给我们发送,可能会出现重复消费的问题
         */
        channel.basicConsume(QUEUENAME,true,new DefaultConsumer(channel){

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.err.println("消费者3收到的消息是:"+new String(body));
            }
        });
    }
}
