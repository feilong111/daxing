package com.daxing.rabbitmq;

import com.daxing.utils.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.nio.charset.StandardCharsets;


public class Sender {
    private static final String EXCHANGENAME = "routingexchange";

    public static void main(String[] args) throws Exception {
        //获取连接
        Connection connection = ConnectionUtils.getConnection();
        //声明通道,就类似于数据库的statment
        Channel channel = connection.createChannel();
        //声明交换机
        channel.exchangeDeclare(EXCHANGENAME, "direct");

        //发送消息
        String msg = "我想让谁接收，谁才能接收,一个消费者也可以带多个名字来接收";
        for (int i = 0; i < 100; i++) {
            channel.basicPublish(EXCHANGENAME, "nengliang", null, (msg + i).getBytes(StandardCharsets.UTF_8));
        }
        //如果用不到就可以关闭了
        channel.close();
        connection.close();
    }
}
