package com.daxing.topic;

import com.daxing.utils.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.nio.charset.StandardCharsets;

public class Sender {
    //声明交换机的名称
    private static final String EXCHANGENAME="topicexchange";

    public static void main(String[] args) throws Exception{
        //建立连接
        Connection connection = ConnectionUtils.getConnection();
        //创建通道
        Channel channel = connection.createChannel();
        //声明交换机
        channel.exchangeDeclare(EXCHANGENAME,"topic");
       //发送消息
        String msg = "通配符的适使用";
        for (int i = 0; i < 100; i++) {
            channel.basicPublish(EXCHANGENAME, "user.dao.update", null, (msg + i).getBytes(StandardCharsets.UTF_8));
        }
       //如果用不到就可以关闭了
        channel.close();
        connection.close();

    }
}
