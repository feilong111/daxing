package com.daxing.utils;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
public class ConnectionUtils {

    public static Connection getConnection() throws Exception {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setVirtualHost("/test");
        connectionFactory.setHost("10.9.12.200");
        connectionFactory.setPort(9090);
        connectionFactory.setUsername("test");
        connectionFactory.setPassword("test");
        return connectionFactory.newConnection();
    }
}
