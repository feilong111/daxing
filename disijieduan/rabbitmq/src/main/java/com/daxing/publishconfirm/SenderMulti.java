package com.daxing.publishconfirm;

import com.daxing.utils.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.nio.charset.StandardCharsets;

public class SenderMulti {
    private static final String EXCHANGENAME = "routingexchange";

    public static void main(String[] args) throws Exception {
        //获取连接
        Connection connection = ConnectionUtils.getConnection();
        //声明通道,就类似于数据库的statment
        Channel channel = connection.createChannel();
        //声明交换机
        channel.exchangeDeclare(EXCHANGENAME, "direct");
        channel.confirmSelect();//开启发布确认
        //发送消息
        String msg = "感觉自己被骗了";

        for (int i = 0; i < 100; i++) {
            channel.basicPublish(EXCHANGENAME, "saoma", null, (msg + i).getBytes(StandardCharsets.UTF_8));
        }
        //如果我们一次性发送很多个消息,我们想批量等待结果,只要有一个失败就都失败,全部成功才成功,失败会抛出异常
        channel.waitForConfirmsOrDie();
        //如果用不到就可以关闭了
        channel.close();
        connection.close();
    }
}
