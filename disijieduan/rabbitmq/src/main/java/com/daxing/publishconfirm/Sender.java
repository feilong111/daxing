package com.daxing.publishconfirm;


import com.daxing.utils.ConnectionUtils;
import com.rabbitmq.client.Channel;

import com.rabbitmq.client.Connection;

import java.nio.charset.StandardCharsets;

public class Sender {
    private static final String EXCHANGENAME = "routingexchange";
    public static void main(String[] args) throws Exception {
        //获取连接
        Connection connection = ConnectionUtils.getConnection();
        //声明通道,就类似于数据库的statment
        Channel channel = connection.createChannel();
        //声明交换机
        channel.exchangeDeclare(EXCHANGENAME, "direct");
        channel.confirmSelect();//开启发布确认
        //发送消息
        String msg = "消息必须存储在队列中";
        int i = 0;
        // for (int i = 0; i < 100; i++) {
        channel.basicPublish(EXCHANGENAME, "saoma", null, (msg + i).getBytes(StandardCharsets.UTF_8));
        // }
        //等待服务器返回响应结果,它只能确认数据到达了交换机,至于有没有队列接受保存消息不管,所以在没有队列的时候 消息会丢失
        boolean confirms = channel.waitForConfirms();
        if (confirms) {
            System.err.println("发送成功");
        }else{
            System.err.println("发送失败");
        }
        //如果用不到就可以关闭了
        channel.close();
        connection.close();
    }

}
