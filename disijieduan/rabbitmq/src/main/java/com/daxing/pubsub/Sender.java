package com.daxing.pubsub;
import com.daxing.utils.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.nio.charset.StandardCharsets;

/**
 * Created by Jackiechan on 2022/10/14 10:53
 *
 * @author Jackiechan
 * @version 1.0
 * @since 1.0
 */
public class Sender {
    private static final String EXCHANGENAME = "fanoutexchange";

    public static void main(String[] args) throws Exception {
        //获取连接
        Connection connection = ConnectionUtils.getConnection();
        //声明通道,就类似于数据库的statment
        Channel channel = connection.createChannel();
        //声明交换机
        channel.exchangeDeclare(EXCHANGENAME, "fanout");

        //发送消息
        String msg = "axiba";
        for (int i = 0; i < 100; i++) {
            channel.basicPublish(EXCHANGENAME, "", null, (msg + i).getBytes(StandardCharsets.UTF_8));
        }
        //如果用不到就可以关闭了
        channel.close();
        connection.close();
    }
}
