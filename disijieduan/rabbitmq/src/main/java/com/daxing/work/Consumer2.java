package com.daxing.work;

import com.daxing.utils.ConnectionUtils;
import com.rabbitmq.client.*;

import java.io.IOException;

//创建一个消费者，来接受生产者发送的信息
 public class Consumer2 {
    private static final String QUEUENAME = "workqueue";
    public static void main(String[] args) throws Exception {
        Connection connection = ConnectionUtils.getConnection();
        Channel channel = connection.createChannel();
        //有可能我们的消费者启动的时候生产者还没有启动,所以可能没有队列,所以消费者也需要声明队列
        //队列可以声明多次,但是实际上只会创建一次,并且注意 多次声明的参数必须一模一样
        channel.queueDeclare(QUEUENAME, false, false, false, null);
        //我们告诉服务器每次最多只给我们1条消息,等我们消费完成并且应答之后才会给我们下一条
        channel.basicQos(1);
      /*
      参数1 从哪个队列中收消息
      参数2 是否自动应答,我们需要应道消息服务器我们有没有收到消息,如果不应该,服务器会认为消息我们没有收到,为了保证消息的可靠性,会不断重试给我们发送,可能会出现重复消费的问题
       */
        channel.basicConsume(QUEUENAME,false,new DefaultConsumer(channel){

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.err.println("消费者收到的消息是:" + new String(body));
                //应答服务器,只应答当前消息
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        });
    }
}


